<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Socialite;

class SocialAuthController extends Controller
{
  public function redirect()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function callback()
    {
        $providerUser = Socialite::driver('facebook')->user();
        $data = array('name'=> $providerUser->getName(), 'avatar'=> $providerUser->getAvatar()) ;

        return view('fbprofile')->with(['data'=>$data]);
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

App::singleton('oauth2', function() {

	$storage = new OAuth2\Storage\Pdo(array('dsn' => 'mysql:dbname=homestead;host=localhost', 'username' => 'homestead', 'password' => 'secret'));
	$server = new OAuth2\Server($storage);

	$server->addGrantType(new OAuth2\GrantType\ClientCredentials($storage));
	$server->addGrantType(new OAuth2\GrantType\UserCredentials($storage));

	return $server;
});


Route::get('/', function () {
    return view('welcome');
});

Route::get('/login', function () {
    return view('signin');
});

Route::post('oauth/token', function()
{
	$bridgedRequest  = OAuth2\HttpFoundationBridge\Request::createFromRequest(Request::instance());
	$bridgedResponse = new OAuth2\HttpFoundationBridge\Response();

	$bridgedResponse = App::make('oauth2')->handleTokenRequest($bridgedRequest, $bridgedResponse);

	return $bridgedResponse;
});

Route::auth();

Route::get('/home', 'HomeController@index');

Route::get('/auth', function (){
	return view('auth.auth');
});

Route::get('/redirect', 'SocialAuthController@redirect');
Route::get('/callback', 'SocialAuthController@callback');

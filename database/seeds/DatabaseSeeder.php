<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(OAuthClientsSeeder::class);
        $this->call(OAuthUsersSeeder::class);
    }
}

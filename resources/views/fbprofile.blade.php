@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <ul>
          <li>{{$data['name']}}</li>
          <li><img src="{{$data['avatar']}}"></li>
        </ul>
    </div>
</div>
@endsection
